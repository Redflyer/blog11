<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Post</title>
</head>
<body>
    <form action="/blog/{{ $post->id }}" method="POST" >
        @method('PUT')
        @csrf
        Title <hr> <textarea name="title" id="" cols="30" rows="5">{{ $post->title }}</textarea> <br>
        <hr> <hr>
        Body <hr> <textarea name="body" id="" cols="30" rows="5">{{ $post->body }}</textarea> <br>
        
        <button type="submit">Update</button>
    </form>
</body>
</html>