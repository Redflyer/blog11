<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="/register" method="POST">
        @csrf
        username
        <input type="text" name="username" value="{{ old('username') }}"><br>
        email
        <input type="email" name="email" value="{{ old('email') }}"><br>
        password
        <input type="password" name="password"><br>

        <button type="submit"> Submit</button>

        <ul>
            @foreach ($errors as $error )
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </form>

</body>
</html>