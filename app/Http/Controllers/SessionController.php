<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{

    public function create()
    {
        return view('session.create');
    }

    public function store()
    {
        $attiributes = request()->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (auth()->attempt($attiributes)){
            return redirect('/');
        }

        session()->regenerate();
        
        return back();
    }

    public function destroy()
    {
        auth()->logout();

        return redirect('/');
    }
}
