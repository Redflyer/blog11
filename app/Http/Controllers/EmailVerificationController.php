<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

use Illuminate\Http\Request;

class EmailVerificationController extends Controller 
{
    public function show()
    {
        return view('auth.verify-email');
    }

    public function request()
    {
        auth()->user();
        return redirect('/');
    }
}

