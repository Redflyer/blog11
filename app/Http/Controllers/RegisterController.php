<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    public function create()
    {
        return view('register.create');
    }

    public function store()
    {
       
        $attributes = request()->validate([
            'username' =>['required', 'min:3', Rule::unique('users', 'username')],
            'email' =>'required|email|unique:users,email',
            'password' =>'required',
        ]);


        $user = User::create($attributes);

        auth()->login($user);
        
        return redirect('/');
    }

    public function handle()
    {
        event(new Registered($user));
    }
}
 