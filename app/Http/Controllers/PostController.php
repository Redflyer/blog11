<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index($slug)
    {
        return Post::where('user_id','!=' ,$slug)->get();
    }

    public function store(Request $request)
    {
        $user = auth()->user();
        $validated = $request->validate([
        'title' => 'required|string|unique:posts,title',
        'body' => 'required|string',
        ]);

        $validated['user_id'] = $user->id;

        $post = Post::create($validated);

        return redirect('/');
    }


    public function destroy(Post $post)
    {
        $post->delete();
        return redirect('/');
    }

    public function edit(Post $post)
    {
        return view ('blog.edit',[
            'post' => $post
        ]);
    }

    public function update(Request $request,Post $post)
    {
        $validated = $request->validate([
            'title' => 'required|string|unique:posts,title',
            'body' => 'required|string',
            ]);
        
        $post->update($validated);
        return redirect('/');
       // dd($post->title);    
    }

}

