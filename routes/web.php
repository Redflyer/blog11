<?php

use App\Http\Controllers\EmailVerificationController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SessionController;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//Auth::routes(['verify' => true]);

Route::get('/', function () {
    return view('welcome',[
        'posts' => Post::all(),
        'users' => User::all(),
    ]);
});

Route::get('data/{user_id}', [PostController::class, 'index']);

Route::get('/register' , [RegisterController::class, 'create'])->middleware('guest');
Route::post('/register' , [RegisterController::class, 'store'])->middleware('guest');

Route::get('/login', [SessionController::class, 'create'])->middleware('guest');
Route::post('sessions', [SessionController::class, 'store'])->middleware('guest');

Route::post('/logout', [SessionController::class, 'destroy'])->middleware('auth');

Route::get('/blog/create', function(){return view('blog.create');})->middleware('auth');
Route::post('/blog/create',[PostController::class, 'store'])->middleware('auth');



Route::delete('/blog/{post:id}', [PostController::class, 'destroy'])->middleware('auth');
Route::get('/blog/{post:id}/edit', [PostController::class, 'edit'])->middleware('auth');
Route::put('/blog/{post:id}',[PostController::class, 'update'])->middleware('auth');

Route::get('/verify-email', [EmailVerificationController::class, 'show'])->middleware('auth')-> name('verification.notice');
Route::post('/verify-email/request', [EmailVerificationController::class, 'request'])
    ->middleware('auth')
    ->name('verification.request');